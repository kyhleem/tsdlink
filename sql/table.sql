
DROP TABLE tsdlink;
CREATE TABLE tsdlink (
  tsdlinkid int,
  nexttsdlinkid int,
  periodtime bigint,
  travelspeed float,
  traveltime float,
  probecount int,
  congestion int,
  primary key (tsdlinkid, nexttsdlinkid)
);
