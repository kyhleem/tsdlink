package com.skt.traffic.tsdlink

import java.sql.{Connection, DriverManager}
import java.text.SimpleDateFormat
import java.util.Date

import com.skt.traffic.tsdlink.models.Tsdlink
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */
class JDBCSink(url: String, user: String, pwd: String, table: String) extends LazyLogging {
  val driver = "com.mysql.jdbc.Driver"

  var startTime: Long = 0
  val dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  def doProcess(iter: Iterator[Tsdlink]): Unit = {
    val arr = iter.toArray
    val startTime = System.currentTimeMillis()
    var connection: Connection = null
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, user, pwd)
      val delStmt = connection.prepareStatement(
        s"""
           | DELETE FROM ${table} WHERE tsdlinkid = ? AND nexttsdlinkid = ?
         """.stripMargin)

      val insStmt = connection.prepareStatement(
        s"""
           | INSERT INTO ${table}(periodtime, tsdlinkid, nexttsdlinkid, travelspeed, traveltime, probecount, congestion)
           | VALUES (?, ?, ?, ?, ?, ?, ?)
          """.stripMargin)
      arr.foreach { data =>
        delStmt.setInt(1, data.tsdlinkid)
        delStmt.setInt(2, data.nexttsdlinkid)
        delStmt.addBatch()
      }
      delStmt.executeBatch()

      arr.foreach { data =>
        insStmt.setLong(1, data.periodtime)
        insStmt.setInt(2, data.tsdlinkid)
        insStmt.setInt(3, data.nexttsdlinkid)
        insStmt.setFloat(4, data.travelspeed)
        insStmt.setFloat(5, data.traveltime)
        insStmt.setInt(6, data.probecount)
        insStmt.setInt(7, data.congestion)
        insStmt.addBatch()
      }
      insStmt.executeBatch()

      val endTime = System.currentTimeMillis()
      println(s"${dateFormat.format(new Date())} : threadId=${Thread.currentThread().getId}, count => ${arr.size}, duration => ${endTime - startTime}ms")

    } finally {
      try { if(connection != null) connection.close() } catch { case e: Throwable => logger.error(e.getMessage, e) }
    }
  }
}