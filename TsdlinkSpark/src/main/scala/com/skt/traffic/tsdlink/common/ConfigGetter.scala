package com.skt.traffic.tsdlink.common

import java.io.File

import com.typesafe.config._
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.{Duration, FiniteDuration}

/**
  * Created by kyu hyoung Lee on 2018-08-14.
  */
object ConfigGetter extends LazyLogging {
  case class Bytes(value: Long)

  type Getter[T] = (Config, String) => T

  lazy val config: Config = {
    val configFile = System.getProperty("CONFIG_FILE", System.getenv("CONFIG_FILE"))
    logger.info("CONFIG_FILE => {}", configFile)

    val cfg = if(configFile != null) {
      val f = new File(configFile)
      if (f.exists()) ConfigFactory.parseFile(f) else ConfigFactory.load(configFile)
    } else ConfigFactory.load()

    val iter = cfg.entrySet().iterator()
    while(iter.hasNext) {
      val map = iter.next()
      logger.info(map.getKey + " = {}", map.getValue.toString)
    }

    cfg
  }

  implicit val stringGetter: Getter[String] = _ getString _
  implicit val booleanGetter: Getter[Boolean] = _ getBoolean _
  implicit val intGetter: Getter[Int] = _ getInt _
  implicit val doubleGetter: Getter[Double] = _ getDouble _
  implicit val longGetter: Getter[Long] = _ getLong _
  implicit val bytesGetter: Getter[Bytes] = (c, p) => Bytes(c getBytes p)
  implicit val durationGetter: Getter[Duration] = (c, p) => Duration.fromNanos((c getDuration p).toNanos)
  implicit val finiteDurationGetter: Getter[FiniteDuration] = (c, p) => Duration.fromNanos((c getDuration p).toNanos)
  implicit val configListGetter: Getter[ConfigList] = _ getList _
  implicit val configGetter: Getter[Config] = _ getConfig _
  implicit val objectGetter: Getter[ConfigObject] = _ getObject _
  implicit val memorySizeGetter: Getter[ConfigMemorySize] = _ getMemorySize _

  implicit class ConfigOps(val config: Config) extends AnyVal {
    def getOrElse[T: Getter](path: String, defValue: => T): T = opt[T](path) getOrElse defValue

    def opt[T: Getter](path: String): Option[T] = {
      if (config hasPathOrNull path) {
        val getter = implicitly[Getter[T]]
        Some(getter(config, path))
      } else
        None
    }
  }
}
