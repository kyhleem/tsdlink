package com.skt.traffic.tsdlink.common

import java.text.SimpleDateFormat

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.Dataset

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Try
import scala.util.control.NonFatal
import ConfigGetter._

/**
  * Created by kyu hyoung Lee on 2018-08-14.
  */
object Common extends LazyLogging {

  object Config {

    // jdbc
    lazy val jdbcDriver: String = config.getString("jdbc.driver")
    lazy val jdbcUrl: String = config.getString("jdbc.url")
    lazy val jdbcUser: String = config.getString("jdbc.user")
    lazy val jdbcPassword: String = config.getString("jdbc.password")

    // hdfs
    lazy val defaultFS: String = config.getString("hdfs.defaultFS")
    lazy val tsdturntrueinfoPath: String = config.getString("hdfs.tsdturntrueinfoPath")
  }

  def printConsole(ds: Dataset[_], numRows: Int = 0): Unit = {
    if (ds.isStreaming) {
      val writer = ds.writeStream.format("console").option("header", "true").option("truncate", false)
      if (numRows > 0) writer.option("numRows", numRows)
      writer.start()
    } else {
      if (numRows > 0) ds.show(numRows, truncate = false) else ds.show(truncate = false)
    }
  }

  def watchTime[T](name: String, min: Int = 0)(block: => T): T = {
    val start = System.nanoTime()
    val ret = block
    val end = System.nanoTime()

    val elapsed = (end - start) nanos

    if (elapsed.toMillis > min) {
      println(s"code $name takes ${elapsed.toMillis} millis seconds.")
    }
    ret
  }

  def isJson(str: String) = str.contains(":") && str.contains("{") && str.contains("}")
  def isTimestamp(str: String) = Try(str.toLong).map(_ => true).getOrElse(false)
  def isDate(str: String) = Try {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val timestamp = dateFormat.parse(str).getTime
    timestamp
  }.map(_ => true).getOrElse(false)

  val utfDf: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
  def toUTFDate(timestamp: Long) = {
    utfDf.format(timestamp)
  }

  def getResourceFile(path: String): String = {
    withResources(scala.io.Source.fromURL(getClass.getResource(path))) { source =>
      source.mkString
    }
  }

  def withResources[T <: {def close(): Unit}, V](r: => T)(f: T => V): V = {
    val resource: T = r
    require(resource != null, "resource is null")
    var exception: Throwable = null
    try {
      f(resource)
    } catch {
      case NonFatal(e) =>
        exception = e
        throw e
    } finally {
      closeAndAddSuppressed(exception, resource)
    }
  }
  private def closeAndAddSuppressed(e: Throwable,
                                    resource: {def close(): Unit}): Unit = {
    if (e != null) {
      try {
        resource.close()
      } catch {
        case NonFatal(suppressed) =>
          e.addSuppressed(suppressed)
      }
    } else {
      resource.close()
    }
  }

  def crossJoin[T](list: Traversable[Traversable[T]]): Traversable[Traversable[T]] =
    list match {
      case xs :: Nil => xs map (Traversable(_))
      case x :: xs => for {
        i <- x
        j <- crossJoin(xs)
      } yield Traversable(i) ++ j
    }

  def withTry[R](f: => R, default: => R): R = {
    try {
      f
    } catch {
      case e: Throwable =>
        logger.error(e.getMessage, e)
        e.printStackTrace()
        default
    }
  }
}
