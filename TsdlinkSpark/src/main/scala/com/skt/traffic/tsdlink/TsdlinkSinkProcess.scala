package com.skt.traffic.tsdlink

import com.skt.traffic.tsdlink.common.Common.Config
import com.skt.traffic.tsdlink.models.Tsdlink
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.{Dataset, SparkSession}

/**
  * Created by kyu hyoung Lee on 2019-08-26.
  */
object TsdlinkSinkProcess extends LazyLogging {

  def createSparkSession(master: Option[String], appName: String, config: Map[String, String] = Map()): SparkSession = {
    val builder = SparkSession.builder.appName(appName)
    config.foreach(kv => builder.config(kv._1, kv._2))
    master.foreach(mst => builder.master(mst))
    builder.getOrCreate()
  }

  def main(args: Array[String]) = {

    val path = if(args.length == 1) args(0) else {
      println("TsdlinkSinkProcess [orc_dir]")
      sys.exit(0)
    }

    val master = if (System.getProperty("spark.master") == null) Some("local[*]") else None
    val spark = createSparkSession(master, "TsdlinkSinkProcess")
    val process = new TsdlinkSinkProcess(spark)
    val ds: Dataset[Tsdlink] = process.read(path)
    println("count => " + ds.count())
    process.write(ds)
  }
}

class TsdlinkSinkProcess(spark: SparkSession) {
  import spark.implicits._

  def orc(path: String) = {
    val df = spark.read.orc(path)
    df.as[Tsdlink]
  }

  def read(path: String) = {
    val ds = orc(path)
      .repartition('tsdlinkid, 'nexttsdlinkid)

    ds.createOrReplaceTempView("tsdturntrueinfo")
    spark.sql(
      """
        | select * from (
        |   select periodtime, tsdlinkid, nexttsdlinkid, travelspeed, traveltime, probecount, congestion,
        |          rank() over (partition by tsdlinkid, nexttsdlinkid order by periodtime desc) time_rank
        |   from tsdturntrueinfo
        | ) a
        | where time_rank = 1
      """.stripMargin
    )
    .as[Tsdlink]
    .cache()
  }

  def write(ds: Dataset[Tsdlink]) = {
    ds
      .repartition( (ds.count() / 1000).toInt + 1 )
      .foreachPartition { iter =>
        val sink = new JDBCSink(Config.jdbcUrl, Config.jdbcUser, Config.jdbcPassword, "tsdlink")
        sink.doProcess(iter)
      }
  }
}
