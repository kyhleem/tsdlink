package com.skt.traffic.tsdlink

import org.scalatest.FunSuite

/**
  * Created by kyu hyoung Lee on 2019-09-09.
  */
class TsdlinkSinkProcessTest extends FunSuite {
  test("main") {

    //val dir = "hdfs://mpp-dev04:8020/tp/raw_data/tsdturntrueinfo/dt=20190905"
    val dir = "hdfs://mpp-dev04:8020/tp/raw_data/tsdturntrueinfo/dt=20190905/tm=10"

    TsdlinkSinkProcess.main(Array(dir))
  }
}
