package com.skt.traffic.tsdlink

import com.skt.traffic.tsdlink.TsdlinkSinkProcess.createSparkSession
import com.skt.traffic.tsdlink.models.Tsdlink
import org.apache.spark.sql.Dataset
import org.scalatest.FunSuite

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */
class TsdTest extends FunSuite {
  val master = if (System.getProperty("spark.master") == null) Some("local[*]") else None
  val spark = createSparkSession(master, "TsdTest")
  import spark.implicits._

  val path = "hdfs://mpp-dev04:8020/tp/raw_data/tsdturntrueinfo/dt=20190614/tm=00"

  test("word count") {
    val process = new TsdlinkSinkProcess(spark)
    val ds: Dataset[Tsdlink] = process.read(path)
    val link =
    ds
      .map { d => (d.tsdlinkid, d.nexttsdlinkid) }
      .distinct()
      .map { d => if(d._1 < d._2) (d._1 + "->" + d._2, 1) else (d._2  + "->" +  d._1, 1) }
      .rdd
      .reduceByKey((a, b) => a + b)

    link
      .sortBy(_._2 * -1)
      .take(100)
      .foreach(println)

    ds
      .filter { d => d.tsdlinkid == 1004901 || d.nexttsdlinkid == 1004901 }
      .show(truncate = false)
  }
}
