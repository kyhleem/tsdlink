package com.skt.traffic.tsdlink.common

import java.sql.DriverManager

import org.scalatest.FunSuite

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */
class CommonTest extends FunSuite {

  import Common._

  test("show tables") {
    Class.forName(Config.jdbcDriver)
    val conn = DriverManager.getConnection(Config.jdbcUrl, Config.jdbcUser, Config.jdbcPassword)
    val rs = conn.createStatement().executeQuery("show tables")
    while(rs.next()) {
      println(rs.getString(1))
    }
    conn.close()
  }
}
