package com.skt.traffic.tsdlink

import com.skt.traffic.tsdlink.common.Common
import org.scalatest.FunSuite

/**
  * Created by kyu hyoung Lee on 2019-10-02.
  */
class TsdPathProcessTest extends FunSuite {

  TsdPathProcess.initDb("com.mysql.jdbc.Driver", "jdbc:mysql://mpp-dev02:3306/tsd?useSSL=false", "mpp", "Mpppw1!@")

  test("TsdPathService") {
    val forward = TsdPathProcess.forward()
    println(forward.links.size)
  }

  test("list") {
    val list = TsdPathProcess.linkList
    list.take(50).foreach(println)
  }

  test("searchPath") {
    Common.watchTime("forward") {
      TsdPathProcess
        .searchForward(10289992, 3)
        .foreach(println)
    }

    Common.watchTime("backward") {
      TsdPathProcess
        .searchBackward(10289992, 3)
        .foreach(println)
    }

    println(TsdPathProcess.forward().links.head)
    println(TsdPathProcess.backward().links.head)
  }
}
