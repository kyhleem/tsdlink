package com.skt.traffic.tsdlink

import com.skt.traffic.tsdlink.models.Tsdlink
import com.skt.traffic.tsdlink.services.TsdPathService
import org.scalatest.FunSuite

/**
  * Created by kyu hyoung Lee on 2019-09-05.
  */
class TsdPathServiceTest extends FunSuite {

  val all = List(
    Tsdlink(periodtime=0, tsdlinkid=1, nexttsdlinkid=2, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=1, nexttsdlinkid=3, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=2, nexttsdlinkid=1, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=2, nexttsdlinkid=3, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=2, nexttsdlinkid=5, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=3, nexttsdlinkid=2, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=3, nexttsdlinkid=4, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=10, nexttsdlinkid=11, travelspeed=0, traveltime=0, probecount=0, congestion=0),
    Tsdlink(periodtime=0, tsdlinkid=10, nexttsdlinkid=2, travelspeed=0, traveltime=0, probecount=0, congestion=0)
  )

  val list = new TsdPathService(all)

  test("links") {
    val link = all(0)
    list.lineList(link, None).foreach(println)
  }

  test("next links") {
    val link1 = all(0)
    val link2 = all(2)

    println("line list")
    list.lineList(link2, None).foreach(println)
    println()
    println("all next")
    list.nextLink(link2, None).foreach(println)
    println()
    println("prev skip ")
    list.nextLink(link2, Some(link1)).foreach(println)
  }

  test("search") {
    val linkid = 1

    list
      .linkMap
      .get(linkid)
      .foreach { opt => opt.foreach(println) }

    println("=========================================================================")
    val paths = list.searchPath(linkid, 3)
    println()
    paths.foreach { path => println(path) }
  }

}
