package com.skt.traffic.tsdlink;

import com.skt.traffic.tsdlink.models.Tsdlink;
import java.util.Iterator;
import java.util.List;
import scala.collection.JavaConversions;

/**
 * Created by kyu hyoung Lee on 2019-09-06.
 */
public class TsdPathProcessJavaTest {

  /**
   *   driver = "com.mysql.jdbc.Driver"
   *   url = "jdbc:mysql://mpp-dev02:3306/tsd?useSSL=false"
   *   user = "mpp"
   *   password = "Mpppw1!@"
   * }
   */
  public static void testTsdlinkList() {
    // init database
    TsdPathProcess.initDb("com.mysql.jdbc.Driver", "jdbc:mysql://mpp-dev02:3306/tsd?useSSL=false", "mpp", "Mpppw1!@");
    scala.collection.immutable.List<Tsdlink> list =  TsdPathProcess.linkList();

    Iterator<Tsdlink> iter = JavaConversions.asJavaIterable(
        list.take(20)
    ).iterator();

    System.out.println("top 20");
    while(iter.hasNext()) {
      Tsdlink link = iter.next();
      System.out.println(link);
    }
  }

  public static void testTsdPathSearch() {
    // init database
    TsdPathProcess.initDb("com.mysql.jdbc.Driver", "jdbc:mysql://mpp-dev02:3306/tsd?useSSL=false", "mpp", "Mpppw1!@");

    System.out.println("forward - 3 step");
    List<List<String>> pathList = TsdPathProcess.forward().searchPathJava(10289992, 3);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

    System.out.println("forward - 4 step");
    pathList = TsdPathProcess.forward().searchPathJava(10289992, 4);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

    //TsdPathProcess.recreateService();
    System.out.println("forward - 5 step");
    pathList = TsdPathProcess.forward().searchPathJava(10289992, 5);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

    System.out.println();

    System.out.println("backward - 3 step");
    pathList = TsdPathProcess.backward().searchPathJava(10289992, 3);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

    System.out.println("backward - 4 step");
    pathList = TsdPathProcess.backward().searchPathJava(10289992, 4);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

    TsdPathProcess.recreateService();
    System.out.println("backward - 5 step");
    pathList = TsdPathProcess.backward().searchPathJava(10289992, 5);
    pathList.stream().forEach(linkList -> System.out.println(linkList));

  }

  public static void main(String[] args) {
    System.out.println("testTsdlinkList");
    System.out.println("============================");
    testTsdlinkList();
    System.out.println("");

    System.out.println("testTsdPathSearch");
    System.out.println("============================");
    testTsdPathSearch();
    System.out.println("");
  }
}
