package com.skt.traffic.tsdlink.models

import scala.collection.mutable._

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */

case class Tsdlink(periodtime: Long, tsdlinkid: Int, nexttsdlinkid: Int, travelspeed: Float, traveltime: Float, probecount: Int, congestion: Int) {
  override def toString: String = s"$tsdlinkid -> $nexttsdlinkid"
}

class TsdPath(val base: Tsdlink) {
  val links = HashSet[Tsdlink]()

  def add(link: Tsdlink) = links += link

  def next(f: Tsdlink => Boolean ): List[Tsdlink] = links.filter(link => f(link)).toList
}
