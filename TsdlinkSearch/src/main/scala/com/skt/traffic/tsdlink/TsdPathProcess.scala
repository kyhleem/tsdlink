package com.skt.traffic.tsdlink

import java.sql.DriverManager

import com.skt.traffic.tsdlink.common.Common
import com.skt.traffic.tsdlink.models.Tsdlink
import com.skt.traffic.tsdlink.services.TsdPathService
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable.ArrayBuffer

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */
object TsdPathProcess extends LazyLogging {
  var jdbcDriver: String = _
  var jdbcUrl: String = _
  var jdbcUser: String = _
  var jdbcPassword: String = _

  def initDb(jdbcDriver: String, jdbcUrl: String, jdbcUser: String, jdbcPassword: String): Unit = {
    this.jdbcDriver = jdbcDriver
    this.jdbcUrl = jdbcUrl
    this.jdbcUser = jdbcUser
    this.jdbcPassword = jdbcPassword
  }

  private var tsdPathServiceTuple: (TsdPathService, TsdPathService) = _

  def forward() = {
    if(tsdPathServiceTuple == null) tsdPathServiceTuple = createService()
    tsdPathServiceTuple._1
  }
  def backward() = {
    if(tsdPathServiceTuple == null) tsdPathServiceTuple = createService()
    tsdPathServiceTuple._2
  }

  def recreateService() = {
    tsdPathServiceTuple = createService()
    tsdPathServiceTuple
  }

  def linkList = forward().links

  def searchForward(linkid: Int, step: Int): List[List[Tsdlink]] = {
    forward().searchPath(linkid, step)
  }

  def searchBackward(linkid: Int, step: Int): List[List[Tsdlink]] = {
    backward().searchPath(linkid, step)
  }

  private def createService(): (TsdPathService, TsdPathService) = {
    val linkList = Common.watchTime("load table(tsdlink)") { load() }
    logger.info(s"tsdlink count => ${linkList.size}")
    (
      new TsdPathService(linkList),  // forward
      new TsdPathService(linkList.map { link => link.copy(tsdlinkid = link.nexttsdlinkid, nexttsdlinkid = link.tsdlinkid) }) // backward
    )
  }

  protected def load() = {
    Class.forName(jdbcDriver)
    val conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword)
    try {
      val pstmp = conn.prepareStatement(
        """
          | select periodtime, tsdlinkid, nexttsdlinkid, travelspeed, traveltime, probecount, congestion
          | from tsdlink
        """.stripMargin)

      val rs = pstmp.executeQuery()

      val list = ArrayBuffer[Tsdlink]()
      while (rs.next()) {
        list += Tsdlink(
          periodtime = rs.getLong("periodtime"),
          tsdlinkid = rs.getInt("tsdlinkid"),
          nexttsdlinkid = rs.getInt("nexttsdlinkid"),
          travelspeed = rs.getFloat("travelspeed"),
          traveltime = rs.getFloat("traveltime"),
          probecount = rs.getInt("probecount"),
          congestion = rs.getInt("congestion"))
      }
      list.toList
    } finally {
      conn.close()
    }
  }

}
