package com.skt.traffic.tsdlink.common

import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.control.NonFatal

/**
  * Created by kyu hyoung Lee on 2018-08-14.
  */
object Common extends LazyLogging {
  def watchTime[T](name: String, min: Int = 0)(block: => T): T = {
    val start = System.nanoTime()
    val ret = block
    val end = System.nanoTime()

    val elapsed = (end - start) nanos

    if (elapsed.toMillis > min) {
      println(s"code $name takes ${elapsed.toMillis} millis seconds.")
    }
    ret
  }

  def getResourceFile(path: String): String = {
    withResources(scala.io.Source.fromURL(getClass.getResource(path))) { source =>
      source.mkString
    }
  }

  def withResources[T <: {def close(): Unit}, V](r: => T)(f: T => V): V = {
    val resource: T = r
    require(resource != null, "resource is null")
    var exception: Throwable = null
    try {
      f(resource)
    } catch {
      case NonFatal(e) =>
        exception = e
        throw e
    } finally {
      closeAndAddSuppressed(exception, resource)
    }
  }
  private def closeAndAddSuppressed(e: Throwable,
                                    resource: {def close(): Unit}): Unit = {
    if (e != null) {
      try {
        resource.close()
      } catch {
        case NonFatal(suppressed) =>
          e.addSuppressed(suppressed)
      }
    } else {
      resource.close()
    }
  }
}
