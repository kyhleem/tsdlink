package com.skt.traffic.tsdlink.services

import com.skt.traffic.tsdlink.models.TsdPath
import com.skt.traffic.tsdlink.models.Tsdlink

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  * Created by kyu hyoung Lee on 2019-09-04.
  */
class TsdPathService(val links: List[Tsdlink]) {

  val linkMap  = links.groupBy(_.tsdlinkid)
  val pathMap = mutable.HashMap[Tsdlink, TsdPath]()
  links.foreach(link => add(link))

  private def add(link: Tsdlink) = {
    if(!pathMap.contains(link)) {
      pathMap.put(link, new TsdPath(link))
    }
    val path = pathMap(link)
    linkMap(link.tsdlinkid).foreach { link => path.add(link) }
  }

  def get(link: Tsdlink) = pathMap.get(link)

  def allTsdlink() = links
  def allTsdlinkJava() = links.asJava

  def lineList(link: Tsdlink, from: Option[Tsdlink] = None) = {
    val path = pathMap(link)
    path.next { link2 => from match {
        case None => true
        case Some(link1) => link1.tsdlinkid != link2.nexttsdlinkid
      }
    }
  }

  def nextLink(link: Tsdlink, from: Option[Tsdlink] = None) = {
    val lines = lineList(link, from)
    lines
      .flatMap { link => linkMap.get(link.nexttsdlinkid).getOrElse(List.empty)}
      .filter { to => link.tsdlinkid != to.nexttsdlinkid }
  }

  def moveStep(paths: List[List[Tsdlink]]) = {
    paths.flatMap { links =>
      val from = if (links.size == 1) None else Some(links(links.size - 2))
      val nextlinks = nextLink(links.last, from)
      val linkids = links.flatMap(link => link.tsdlinkid :: link.nexttsdlinkid :: Nil).distinct

      nextlinks
        .filter { next => links.last.nexttsdlinkid == next.tsdlinkid }
        .filter { next => !linkids.contains(next.nexttsdlinkid) }
        .map { next => links :+ next }
    }
  }

  def searchPath(linkid: Int, step: Int) = {
    def loop(paths: List[List[Tsdlink]], cnt: Int): List[List[Tsdlink]] = {
      if(cnt <= 0) paths
      else loop(moveStep(paths), cnt - 1)
    }

    linkMap
      .get(linkid)
      .map { list => list.map( link => List(link))  } match {
        case Some(all) => loop(all, step - 1)
        case None => List.empty
      }
  }

  def searchPathJava(linkid: Int, step: Int) = {
    searchPath(linkid, step)
      .map(list => list.map(_.nexttsdlinkid.toString))
      .distinct
      .map(_.asJava)
      .asJava
  }
}
